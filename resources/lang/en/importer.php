<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dendev\Importer Translation Lines
    |--------------------------------------------------------------------------
    */
    'field_label' => 'label',
    'field_description' => 'description',
    'field_type' => 'type',
    'field_transformers' => 'transforms',
    'field_input_fields' => 'input fields',
    'field_output_fields' => 'output fields',
    'field_worker' => 'worker',
    'field_created_at' => 'created at',
    'field_updated_at' => 'update at',

    'field_transformer_key' => '#',
    'field_transformer_type' => 'type',
    'field_transformer_args' => 'args',
    'field_transformer_args_hint' => 'be carrefoul args will be transformer in array. Write it in json.',
    'field_transformer_new' => 'add',

    'field_transformer_trim' => 'trim',
    'field_transformer_ucfirst' => 'ucfirst',
    'field_transformer_replace' => 'replace',
    'field_transformer_remove' => 'remove',
    'field_transformer_implode' => 'implode',
    'field_transformer_amount' => 'amount',

    'fields_key' => '#',
    'fields_identity' => 'identity',
    'fields_name' => 'result',
    'fields_input' => 'input',
    'fields_output' => 'output',

    'field_input_key' => '#',
    'field_input_field_identity' => 'identity',
    'field_input_field_identity_hint' => 'unique identifiant',
    'field_input_field_name' => 'name',
    'field_input_field_name_hint' => 'field name in source',
    'field_input_field_is_mandatory' => 'is mandatory',
    'field_input_field_is_mandatory_yes' => 'yes',
    'field_input_field_is_mandatory_no' => 'no',
    'field_input_field_new' => 'new',

    'field_output_key' => '#',
    'field_output_field_identity' => 'identity',
    'field_output_field_identity_hint' => 'same unique identifiant defined in input field',
    'field_output_field_name' => 'name',
    'field_output_field_name_hint' => 'field name in output',
    'field_output_field_is_mandatory' => 'is mandatory',
    'field_output_field_is_mandatory_yes' => 'yes',
    'field_output_field_is_mandatory_no' => 'no',
    'field_output_field_new' => 'new',

    'field_transformer_identity' => 'identity',
    'field_transformer_identity_hint' => 'same unique identifiant defined in input field',
    'field_transformer_priority' => 'priority',

    'field_worker_hint' => "FullClassName::name of static method who treat output fields",

    // Operations
    'operation_title' => 'import from file',
    'operation_alert_excel' => 'add Excel file ( xls or xlsx )',
    'operation_alert_csv' => 'add csv file',
    'operation_btn_close' => 'close',
    'operation_btn_send' => 'send',

    'operation_run_action' => 'run',
    'operation_run_success' => 'success datas imported',
    'operation_run_fail' => 'no datas extracted and imported',

    'operation_test_action' => 'test',
    'operation_test_result_no_datas' => 'no datas',
    'operation_test_result_datas' => 'results',
    'operation_test_result_debug' => 'debug',
    'operation_test_result_debug_datas' => 'datas',
    'operation_test_result_debug_extractor' => 'extractors',
    'operation_test_result_debug_transformers' => 'transformers',
    'operation_test_result_debug_loader' => 'loaders',
];
