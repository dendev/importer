<?php
if( $entry->type === 'excel')
{
    $alert_msg = ucfirst(trans('dendev.importer::importer.operation_alert_excel'));
    $accept = ".xlsx,.xls";
}
else
{
    $alert_msg = ucfirst(trans('dendev.importer::importer.operation_alert_csv'));
    $accept = ".csv";
}
?>
@if ($crud->hasAccess('run'))

    <button type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#importerRunModal">
        <i class="las la-play"></i> {{ucfirst(trans('dendev.importer::importer.operation_run_action'))}}
    </button>
@endif

<!-- Modal -->
<div class="modal" id="importerRunModal" tabindex="-1" role="dialog" aria-labelledby="importerRunModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <form action="{{route('importer.run',['id' => $entry->id])}}" method="post" enctype="multipart/form-data">
            @csrf

            <input type="hidden" name="id" value="{{$entry->id}}">

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="importerModalTitle">
                        {{ucfirst(trans('dendev.importer::importer.operation_title'))}}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" role="alert">
                        {{$alert_msg}}
                    </div>
                    <input type="file" name="fileToUpload" id="fileToUpload" accept="{{$accept}}" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        {{ucfirst(trans('dendev.importer::importer.operation_btn_close'))}}
                    </button>
                    <button type="submit" class="btn btn-primary">
                        {{ucfirst(trans('dendev.importer::importer.operation_btn_send'))}}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
