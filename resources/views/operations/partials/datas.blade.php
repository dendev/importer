<div class="">
    <h2>{{ucfirst(trans('dendev.importer::importer.operation_test_result_datas'))}}</h2>
    @if ($crud->model->translationEnabled())
        <div class="row">
            <div class="col-md-12 mb-2">
                <!-- Change translation button group -->
                <div class="btn-group float-right">
                    <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[request()->input('locale')?request()->input('locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @foreach ($crud->model->getAvailableLocales() as $key => $locale)
                            <a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}?locale={{ $key }}">{{ $locale }}</a>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    <div class="card no-padding no-border">
        @if( count( $datas ) > 0)
            @php $heads = array_keys($datas[0]); @endphp
            <table class="table table-striped mb-0">
                <thead>
                <tr>
                    @foreach ($heads as $head )
                        <th>
                            <strong>{{ $head }}</strong>
                        </th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach ($datas as $data)
                    <tr>
                        @foreach ($heads as $key )
                            <td>
                                @if( array_key_exists($key, $data) )
                                {{$data[$key] }}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                @if ($crud->buttons()->where('stack', 'line')->count())
                    <tr>
                        <td><strong>{{ trans('backpack::crud.actions') }}</strong></td>
                        <td>
                            @include('crud::inc.button_stack', ['stack' => 'line'])
                        </td>
                    </tr>
                @endif
                </tbody>
                <tfoot>
                <tr>
                    @foreach ($heads as $head )
                        <th>
                            <strong>{{ $head }}</strong>
                        </th>
                    @endforeach
                </tr>
                </tfoot>
            </table>
        @else
            <div class="alert alert-warning m-3" role="alert">
                {{ucfirst(trans('dendev.importer::importer.operation_test_result_no_datas'))}}
            </div>
        @endif
    </div><!-- /.box-body -->
</div><!-- /.box -->
