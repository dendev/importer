<div class="row">
    <div class="col-12">
        <div class="">
            <h2>{{ucfirst(trans('dendev.importer::importer.operation_test_result_debug'))}}</h2>
            <div class="card no-padding no-border">
                <div class="accordion" id="accordionDebugDatas">
                    <!-- datas -->
                    <div class="card">
                        <div class="card-header" id="headingDatas">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseDatas" aria-expanded="false" aria-controls="collapseDatas">
                                    {{ucfirst(trans('dendev.importer::importer.operation_test_result_debug_datas'))}}
                                </button>
                            </h2>
                        </div>

                        <div id="collapseDatas" class="collapse" aria-labelledby="headingDatas" data-parent="#accordionDebugDatas">
                            <div class="card-body">
                                <pre>
                                    @dump($datas)
                                </pre>
                            </div>
                        </div>
                    </div>
                    <!-- extractor -->
                    <div class="card">
                        <div class="card-header" id="headingExtractor">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseExtractor" aria-expanded="false" aria-controls="collapseExtractor">
                                    {{ucfirst(trans('dendev.importer::importer.operation_test_result_debug_extractor'))}}
                                </button>
                            </h2>
                        </div>
                        <div id="collapseExtractor" class="collapse" aria-labelledby="headingExtractor" data-parent="#accordionDebugDatas">
                            <div class="card-body">
                                <pre>
                                    @dump($importer->extractor_fields)
                                </pre>
                            </div>
                        </div>
                    </div>
                    <!-- transformers -->
                    <div class="card">
                        <div class="card-header" id="headingTransformers">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTransformers" aria-expanded="false" aria-controls="collapseTransformers">
                                    {{ucfirst(trans('dendev.importer::importer.operation_test_result_debug_transformers'))}}
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTransformers" class="collapse" aria-labelledby="headingTransformers" data-parent="#accordionDebugDatas">
                            <div class="card-body">
                                <pre>
                                    @dump($importer->transformer)
                                </pre>
                            </div>
                        </div>
                    </div>
                    <!-- loaders -->
                    <div class="card">
                        <div class="card-header" id="headingLoader">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseLoader" aria-expanded="false" aria-controls="collapseLoader">
                                    {{ucfirst(trans('dendev.importer::importer.operation_test_result_debug_loader'))}}
                                </button>
                            </h2>
                        </div>
                        <div id="collapseLoader" class="collapse" aria-labelledby="headingLoader" data-parent="#accordionDebugDatas">
                            <div class="card-body">
                                <pre>
                                    @dump($importer->loader_fields)
                                </pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

