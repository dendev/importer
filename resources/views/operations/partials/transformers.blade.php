<div class="row">
    <div class="col-12">
        <div class="">
            <h2>{{ucfirst(trans('dendev.importer::importer.field_transformers'))}}</h2>
            <div class="card no-padding no-border">
                @if( count( $datas ) > 0)
                    @php $heads = array_keys($datas[0]); @endphp
                    <table class="table table-striped mb-0">
                        <thead>
                        <tr>
                            <th>
                                {{ucfirst(trans('dendev.importer::importer.field_transformer_identity'))}}
                            </th>
                            <th>
                               {{ ucfirst(trans('dendev.importer::importer.field_transformer_type'))}}
                            </th>
                            <th>
                                {{ ucfirst(trans('dendev.importer::importer.field_transformer_args'))}}
                            </th>
                            <th>
                                {{ ucfirst(trans('dendev.importer::importer.field_transformer_priority'))}}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($importer->transformers as $identifiant => $transformer)
                                <tr>
                                    <td>
                                        {{ $transformer['identity'] }}
                                    </td>
                                    <td>
                                        {{ $transformer['type'] }}
                                    </td>
                                    <td>
                                        {{ $transformer['args'] }}
                                    </td>
                                    <td>
                                        {{ $transformer['priority'] }}
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>
                                {{ucfirst(trans('dendev.importer::importer.field_transformer_identity'))}}
                            </th>
                            <th>
                                {{ ucfirst(trans('dendev.importer::importer.field_transformer_type'))}}
                            </th>
                            <th>
                                {{ ucfirst(trans('dendev.importer::importer.field_transformer_args'))}}
                            </th>
                            <th>
                                {{ ucfirst(trans('dendev.importer::importer.field_transformer_priority'))}}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                @else
                    <div class="alert alert-warning m-3" role="alert">
                        {{ucfirst(trans('dendev.importer::importer.operation_test_result_no_datas'))}}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
