@php
    $fields = ( count( $entry->{$column['name']} ) > 0 ) ? $entry->{$column['name']} : false;
@endphp

@if( $fields )
    <span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
        <thead>
        <tr>
            <th>{{ucfirst(trans('dendev.importer::importer.field_output_key'))}}</th>
            <th>{{ucfirst(trans('dendev.importer::importer.field_output_field_name'))}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($fields as $key => $field)
            <tr>
                <td>
                    {{$key}}
                </td>
            <td>
                {{$field['name']}}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </span>
@else
@endif
