@php
    $input_fields = $entry->input_fields;
    $output_fields = $entry->output_fields;
@endphp

@if( $input_fields )
    <span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
        <thead>
        <tr>
            <th>{{ucfirst(trans('dendev.importer::importer.fields_key'))}}</th>
            <th>{{ucfirst(trans('dendev.importer::importer.fields_identity'))}}</th>
            <th>{{ucfirst(trans('dendev.importer::importer.fields_input'))}}</th>
            <th>{{ucfirst(trans('dendev.importer::importer.fields_output'))}}</th>
            <th>{{ucfirst(trans('dendev.importer::importer.field_input_field_is_mandatory'))}}</th>
        </tr>
        </thead>
        <tbody>
        @for($i = 0; $i < count( $input_fields ); $i++)
        <tr>
            <td>
                {{$i}}
            </td>
            <td>
                {{$input_fields[$i]['identity']}}
            </td>
            <td>
                {{$input_fields[$i]['name']}}
            </td>
            <td>
                @if( array_key_exists($i, $output_fields))
                    {{$output_fields[$i]['name']}}
                @else
                    -
                @endif
            </td>
            <td>
                {{$input_fields[$i]['is_mandatory']}}
            </td>
        </tr>
        @endfor
        </tbody>
    </table>
    </span>
@else
@endif
