@php
$transformers = ( count( $entry->transformers ) > 0 ) ? $entry->transformers : false;
@endphp

@if( $transformers )
    <span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
        <thead>
        <tr>
            <th>{{ucfirst(trans('dendev.importer::importer.field_transformer_key'))}}</th>
            <th>{{ucfirst(trans('dendev.importer::importer.field_transformer_identity'))}}</th>
            <th>{{ucfirst(trans('dendev.importer::importer.field_transformer_type'))}}</th>
            <th>{{ucfirst(trans('dendev.importer::importer.field_transformer_args'))}}</th>
            <th>{{ucfirst(trans('dendev.importer::importer.field_transformer_priority'))}}</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($transformers as $key => $transformer)
                <tr>
                    <td>
                        {{ $key }}
                    </td>
                    <td>
                        {{ $transformer['identity'] }}
                    </td>
                    <td>
                        {{ $transformer['type'] }}
                    </td>
                    <td>
                        {{ $transformer['args'] }}
                    </td>
                    <td>
                        {{ $transformer['priority'] }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </span>
@else

@endif


