# Importer

> import datas with integration in backpack laravel

## Install
```bash
composer config repositories.importer vcs https://gitlab.com/dendev/importer.git
composer config repositories.etl vcs https://gitlab.com/dendev/etl.git
composer require dendev/importer
```

## Usage 

Run
```bash
php artisan migrate
```
Add in **resources/views/vendor/backpack/base/inc/sidebar_content.blade.php** 
```
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('importer') }}'><i class='nav-icon la la-upload'></i> Importers</a></li>
```
Go to admin dashboard and create a new importer.   

## Concept
Importer can extract datas, transform.  
The result is give to a worker.  
Worker is an static method who use datas. Worker is not a part of importer. Is from client projet and must return true or false.
