<?php

namespace Dendev\Importer\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Importer extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'importers';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = [
        'transformers' => 'array',
        'input_fields' => 'array',
        'output_fields' => 'array'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getExtractorFieldsAttribute($value)
    {
        $fields = [];

        $input_fields = $this->attributes['input_fields'];
        if( $input_fields )
        {
            $input_fields = json_decode($input_fields, true);

            foreach( $input_fields as $input_field )
            {
                $key = $input_field['identity'];
                $rules = ( $input_field['is_mandatory'] ) ? ['required'] : [];

                $field = [
                    'name' => $input_field['name'],
                    'rules' => $rules
                ];

                $fields[$key] = $field;
            }
        }

        return $fields;
    }

    public function getLoaderFieldsAttribute($value)
    {
        $fields = [];

        $output_fields = $this->attributes['output_fields'];
        if( $output_fields )
        {
            $output_fields = json_decode($output_fields, true);

            foreach( $output_fields as $output_field )
            {
                $key = $output_field['identity'];

                $field = [
                    'name' => $output_field['name'],
                ];

                $fields[$key] = $field;
            }
        }

        return $fields;
    }

    public function getTransformerAttribute($value)
    {
        $transformers = [];

        $transforms = $this->attributes['transformers'];
        if( $transforms )
        {
            $transforms = json_decode($transforms, true);

            foreach( $transforms as $transform )
            {
                $key = $transform['identity'];
                $priority = $transform['priority'];

                $transformer = [
                    'type' => $transform['type'],
                    'args' => json_decode($transform['args'], true),
                    'priority' => $priority
                ];

                $transformers[$key][$priority] = $transformer;
            }
            ksort($transformers[$key]);
        }

        return $transformers;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
