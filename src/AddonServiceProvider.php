<?php

namespace Dendev\Importer;

use Illuminate\Support\ServiceProvider;

class AddonServiceProvider extends ServiceProvider
{
    use AutomaticServiceProvider;

    protected $vendorName = 'dendev';
    protected $packageName = 'importer';
    protected $commands = [];
}
