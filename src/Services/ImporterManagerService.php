<?php

namespace Dendev\Importer\Services;


use Dendev\Importer\Models\Importer;
use Dendev\Importer\Traits\UtilService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Exception\CommandNotFoundException;

/**
 * Class EtlManagerService
 * @package Dendev\Etl
 */
class ImporterManagerService
{
    use UtilService;

    /**
     * check if service is accessible
     * @return bool
     */
    public function test_me()
    {
        return true;
    }

    public function run($id_or_model_importer, $src, $fake = false)
    {
        $result = [];
        $result['success'] = false;
        $result['msg'] = ucfirst(trans('dendev.importer::importer.operation_import'));

        $importer = $this->_instantiate_if_id($id_or_model_importer, Importer::class);
        if( $importer )
        {
            $is_valid_importer = $this->_check_importer($importer);
            if( $is_valid_importer )
            {
                $datas = $this->_run_etl($importer, $src);
                if( $fake )
                {
                    $result = [
                        'success' => true,
                        'msg' => ucfirst(trans('dendev.importer::importer.operation_import_fake_success')),
                        'datas' => $datas
                    ];
                }
                else
                {
                    $is_success = $this->_call_worker($importer->worker, $datas);

                    $success = $is_success ? true : false;
                    $msg = $is_success ? ucfirst(trans('dendev.importer::importer.operation_run_success')) : ucfirst(trans('dendev.importer::importer.operation_run_fail'));

                    $result = [
                        'success' => $success,
                        'msg' => $msg,
                        'datas' => $datas
                    ];
                }
            }
        }

        return $result;
    }

    private function _check_importer($importer)
    {
        $is_valid = false;

        if( $importer->type )
        {
            if( is_array($importer->input_fields) && count( $importer->input_fields) > 0)
            {
                if( is_array($importer->output_fields) && count( $importer->output_fields) > 0)
                {
                    if( $this->_check_worker($importer->worker))
                    {
                        $is_valid = true;
                    }
                    else
                    {
                        \Log::error("[Importer::ImporterManager::_check_importer] Not valid worker", [
                            "importer" => $importer
                        ]);
                    }
                }
                else
                {
                    \Log::error("[Importer::ImporterManager::_check_importer] No valid output fields", [
                        "importer" => $importer
                    ]);
                }
            }
            else
            {
                \Log::error("[Importer::ImporterManager::_check_importer] No valid input fields", [
                    "importer" => $importer
                ]);
            }
        }
        else
        {
            \Log::error("[Importer::ImporterManager::_check_importer] No type found", [
                "importer" => $importer
            ]);
        }

        return $is_valid;
    }

    private function _run_etl($importer, $src)
    {
        $extractor = [
            'type' => $importer->type,
            'src' => $src,
            'fields' => $importer->extractor_fields
        ];

        $transformer = $importer->transformer;

        $loader = [
            'type' => 'multi_array',
            'fields' => $importer->loader_fields
        ];

        $datas = \EtlManager::run($extractor, $transformer, $loader);

        return $datas;
    }

    private function _check_worker($worker)
    {
        $is_valid = false;

        $wk =  $this->_get_classname_and_method($worker);

        if( $wk )
        {
            $is_valid_class = class_exists($wk['classname']);
            $is_valid_method = method_exists($wk['classname'], $wk['method']);
        }

        if( $is_valid_class && $is_valid_method )
            $is_valid = true;

        return $is_valid;
    }

    private function _get_classname_and_method($worker)
    {
        $ok = false;

        $tmp = explode('::', $worker);
        if( count( $tmp ) === 2 )
        {
            $ok = [
                'classname' => $tmp[0],
                'method' => $tmp[1],
            ];
        }
        else
        {
            \Log::error("[Importer::ImporterManager::_get_classname_and_method] No valid worker. Must be like full_classname::method.", [
                'worker' =>$worker,
            ]);
        }

        return $ok;
    }

    private function _call_worker($worker, $datas)
    {
        $ok = $worker($datas);

        return $ok;
    }
}
