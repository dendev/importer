<?php

namespace Dendev\Importer\Http\Controllers\Admin;

use Dendev\Importer\Http\Requests\ImporterRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ImporterCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ImporterCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Dendev\Importer\Http\Controllers\Admin\Operations\RunOperation;
    use \Dendev\Importer\Http\Controllers\Admin\Operations\TestOperation;


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\Dendev\Importer\Models\Importer::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/importer');
        CRUD::setEntityNameStrings('importer', 'importers');

        CRUD::allowAccess('run');
        CRUD::allowAccess('test');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.importer::importer.field_label') ),
            'name' => 'label',
            'type' => 'text',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.importer::importer.field_description') ),
            'name' => 'description',
            'type' => 'text',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.importer::importer.field_type') ),
            'name' => 'type',
            'type' => 'text',
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.importer::importer.field_label') ),
            'name' => 'label',
            'type' => 'text',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.importer::importer.field_description') ),
            'name' => 'description',
            'type' => 'text',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.importer::importer.field_type') ),
            'name' => 'type',
            'type' => 'text',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.importer::importer.fields_name') ),
            'name' => 'fields',
            'type' => 'view',
            'view'  => 'dendev.importer::columns.fields',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.importer::importer.field_transformers') ),
            'name' => 'transformers',
            'type' => 'view',
            'view'  => 'dendev.importer::columns.transformers',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.importer::importer.field_worker') ),
            'name' => 'worker',
            'type' => 'text',
        ]);
    }


    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ImporterRequest::class);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.importer::importer.field_label') ),
            'name' => 'label',
            'type' => 'text',
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.importer::importer.field_description') ),
            'name' => 'description',
            'type' => 'textarea',
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.importer::importer.field_type') ),
            'name' => 'type',
            'type'        => 'select2_from_array',
            'options'     => ['excel' => 'Excel', 'csv' => 'Csv'],
            'allows_null' => false,
            'default'     => 'excel',
        ]);



        CRUD::addField([
            'label' => ucfirst( trans('dendev.importer::importer.field_input_fields') ),
            'name' => 'input_fields',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'identity',
                    'type'    => 'text',
                    'label'   => ucfirst( trans('dendev.importer::importer.field_input_field_identity') ),
                    'hint'    => ucfirst( trans('dendev.importer::importer.field_input_field_identity_hint')),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'name',
                    'type'    => 'text',
                    'label'   => ucfirst( trans('dendev.importer::importer.field_input_field_name') ),
                    'hint'    => ucfirst( trans('dendev.importer::importer.field_input_field_name_hint')),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'is_mandatory',
                    'label'   => ucfirst( trans('dendev.importer::importer.field_input_field_is_mandatory') ),
                    'type'    => 'radio',
                    'default' => 1,
                    'options' => [
                        // the key will be stored in the db, the value will be shown as label;
                        0 => ucfirst( trans('dendev.importer::importer.field_input_field_is_mandatory_no') ),
                        1 => ucfirst( trans('dendev.importer::importer.field_input_field_is_mandatory_yes') ),
                    ],
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ]
            ],

            // optional
            'new_item_label'  => ucfirst( trans('dendev.importer::importer.field_input_field_new') ),

        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.importer::importer.field_output_fields') ),
            'name' => 'output_fields',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'identity',
                    'type'    => 'text',
                    'label'   => ucfirst( trans('dendev.importer::importer.field_output_field_identity') ),
                    'hint'    => ucfirst( trans('dendev.importer::importer.field_output_field_identity_hint')),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'name',
                    'type'    => 'text',
                    'label'   => ucfirst( trans('dendev.importer::importer.field_output_field_name') ),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
            ],

            // optional
            'new_item_label'  => ucfirst( trans('dendev.importer::importer.field_output_field_new') ),
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.importer::importer.field_transformers') ),
            'name' => 'transformers',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'identity',
                    'type'    => 'text',
                    'label'   => ucfirst( trans('dendev.importer::importer.field_transformer_identity') ),
                    'hint'    => ucfirst( trans('dendev.importer::importer.field_transformer_identity_hint')),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'type',
                    'type'    => 'select2_from_array',
                    'label'   => ucfirst( trans('dendev.importer::importer.field_transformer_type') ),
                    'options' => [
                        'amount' => ucfirst( trans('dendev.importer::importer.field_transformer_amount') ),
                        'implode' => ucfirst( trans('dendev.importer::importer.field_transformer_implode') ),
                        'remove' => ucfirst( trans('dendev.importer::importer.field_transformer_remove') ),
                        'replace' => ucfirst( trans('dendev.importer::importer.field_transformer_replace') ),
                        'trim' => ucfirst( trans('dendev.importer::importer.field_transformer_trim') ),
                        'ucfirst' => ucfirst( trans('dendev.importer::importer.field_transformer_ucfirst') ),
                    ],
                    'allows_null' => true,
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'args',
                    'label'   => ucfirst( trans('dendev.importer::importer.field_transformer_args') ),
                    'type'    => 'textarea',
                    'hint'    => ucfirst( trans('dendev.importer::importer.field_transformer_args_hint') ),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'priority',
                    'type'    => 'number',
                    'label'   => ucfirst( trans('dendev.importer::importer.field_transformer_priority') ),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
            ],

            // optional
            'new_item_label'  => ucfirst( trans('dendev.importer::importer.field_transformer_new') ),

        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.importer::importer.field_worker') ),
            'name' => 'worker',
            'type' => 'text',
            'hint' => ucfirst( trans('dendev.importer::importer.field_worker_hint') ),
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
