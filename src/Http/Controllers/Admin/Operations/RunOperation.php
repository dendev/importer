<?php

namespace Dendev\Importer\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait RunOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupRunRoutes($segment, $routeName, $controller)
    {
        Route::post($segment.'/run/{id}', [
            'as'        => $routeName.'.run',
            'uses'      => $controller.'@run',
            'operation' => 'run',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupRunDefaults()
    {
        $this->crud->allowAccess('run');

        $this->crud->operation('run', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'run', 'view', 'dendev.importer::buttons.run', 'begin');
        });
        $this->crud->operation('show', function () {
            $this->crud->addButton('line', 'run', 'view', 'dendev.importer::buttons.run', 'begin');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function run($id)
    {
        // check
        $this->crud->hasAccessOrFail('run');

        // args
        $file = request()->file('fileToUpload');

        // action
        $result = \ImporterManager::run($id, $file->path(), false);

        // inform
        if( $result['success'])
        {
            \Alert::success($result['msg'])->flash();
        }
        else
        {
            \Alert::warning($result['msg'])->flash();
        }

        // redirect
        $previous_url = url()->previous();
        return \Redirect::to($previous_url);
    }
}
