<?php

namespace Dendev\Importer\Http\Controllers\Admin\Operations;

use Dendev\Importer\Models\Importer;
use Illuminate\Support\Facades\Route;

trait TestOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupTestRoutes($segment, $routeName, $controller)
    {
        Route::post($segment.'/test/{id}', [
            'as'        => $routeName.'.test',
            'uses'      => $controller.'@test',
            'operation' => 'test',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupTestDefaults()
    {
        $this->crud->allowAccess('test');

        $this->crud->operation('test', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'test', 'view', 'dendev.importer::buttons.test', 'end');
        });
        $this->crud->operation('show', function () {
            $this->crud->addButton('line', 'test', 'view', 'dendev.importer::buttons.test', 'end');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     */
    public function test($id)
    {
        // check
        $this->crud->hasAccessOrFail('test');

        $is_fake = request()->get('is_fake');
        $file = request()->file('fileToUpload');

        // action
        $result = \ImporterManager::run($id, $file->path(), $is_fake);

        // inform
        if( $result['success'] )
        {
            $importer = Importer::find($id);
            return view('dendev.importer::operations.result', [
                    'crud' => $this->crud,
                    'datas' => $result['datas'],
                    'importer' => $importer,
                ]
            );
        }
        else
        {
            \Alert::warning($result[msg])->flash();
            $url = url()->previous();
            return \Redirect::to($url);
        }

        // redirect
    }
}
