<?php

namespace Dendev\Importer\Facades;

use Illuminate\Support\Facades\Facade;

class ImporterManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'importer_manager';
    }
}
