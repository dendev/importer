<?php

/*
|--------------------------------------------------------------------------
| Dendev\Importer Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the Dendev\Importer package.
|
*/

/**
 * User Routes
 */

// Route::group([
//     'middleware'=> array_merge(
//     	(array) config('backpack.base.web_middleware', 'web'),
//     ),
// ], function() {
//     Route::get('something/action', \Dendev\Importer\Http\Controllers\SomethingController::actionName());
// });


/**
 * Admin Routes
 */

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'Dendev\Importer\Http\Controllers\Admin',
], function () {
    Route::crud('importer', 'ImporterCrudController');
});
