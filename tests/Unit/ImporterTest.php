<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class ImporterManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Importer\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    //
    public function testBasic()
    {
        $exist = \ImporterManager::test_me();
        $this->assertTrue($exist);
    }

    public function testCreateImporter()
    {

    }

    public function testEmptyImporter()
    {
        $exist = \ImporterManager::test_me();
        $this->assertTrue($exist);
    }
}
